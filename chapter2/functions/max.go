// Пакет function содержит все пользовательские функции, которые используются в
// заданиях. 
package functions

// Функция вычисления максимума в одномерном массиве. 
/*
Input:
    array   - одномерный массив чисел
    c       - (optional) канал для записи результата
Output:
    max - максимум массива
*/
func Maximum(array []float64, c ...chan<- float64) (max float64) {
    max = array[0]
    for _, v := range array {
        if v > max {
            max = v 
        }
    }
    if len(c) > 0 {
        c[0] <- max
    }

    return max
}