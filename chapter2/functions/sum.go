package functions

func Sum(args ...float64) float64 {
	var sum float64 = 0
	for _, value := range args {
		sum += value
	}
	return sum
}