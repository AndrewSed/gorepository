package functions

// Функция вычисления минимума в одномерном массиве.
/*
Input: 
    array   - одномерный массив чисел
    c       - (optional) канал для записи результата
Output:
    float64 - минимум массива
*/
func Minimum(array []float64, c ...chan<- float64) (min float64) {
    min = array[0]
    for _, v := range array {
        if v < min {
            min = v 
        }
    }
    if len(c) > 0 {
        c[0] <- min
    }

    return min
}
