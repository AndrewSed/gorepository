package functions


import (
	"os"
    "io/ioutil"
    "sort"
    "errors"
)
type FileInfo []os.FileInfo

// Получение файлов из директории.
/*
Input:
    
Output: 
    
*/
func (filesDir FileInfo) getFiles() (files FileInfo, err error) {
    for _, fi := range filesDir {
        if !fi.IsDir() {
            files = append(files, fi)
        }
    }
    if len(files) == 0 {
        err = errors.New("Error, there are no files in the specified folder! ")
    }
    return files, err
}

// Сортировка файлов по размерув в порядке убывания.
/*
Input:
    
Output: 
    
*/
func (filesDir FileInfo) sortBySizeDesc() {
    sort.SliceStable(filesDir, func(i, j int) bool {
        return filesDir[i].Size() > filesDir[j].Size()
    })
}

// Функция поиска файлов с максимальным размером. 
// Функции скрамлевается папка, в которой необходимо найти файлы. 
// Для работы с файловой системой использется пакет io/ioutil. 
// С помощью функции ioutil.ReadDir(string) получаем список всех файлов и 
// дирректорий по указанному пути. 
// Следующим шагом составляется выборка файлов с помощью функции getFiles(). 
// После этого список файлов сортируется по размеру в порядке убылвания  
// с помощью функции sortBySizeDesc(). Эта функция не возвращает никаких 
// значений, а сортирует список типа io.FileInfo, для которого была вызванна. 
// Далее из этого списка исключаются все элементы с размером меньше 
// максимального
/*
Input:
    path - путь до директории
Output:
    files   - список искомых файлов 
    err     - статус ошибки
*/
func FindMaxFileSize(pathDir string) (files FileInfo, err error) {

    fls, err := ioutil.ReadDir(pathDir)
    if err != nil {
        err = errors.New("Error, failed to open dir")
        return files, err
    }
    
    if fls, err = FileInfo(fls).getFiles(); err != nil {
        return files, err
    }
    FileInfo(fls).sortBySizeDesc()

    for i, fi := range fls {
        if fi.Size() < fls[0].Size() {
            files = fls[:i]
            break
        }
    }

    return files, err
}
