package main

import (
    "fmt"
    "golang-book/chapter2/functions"
)

func main() {
    args := []float64{ 22, 43, 90, 45, 35 }
    fmt.Println("Med", args, "=", functions.Med(args...))
}