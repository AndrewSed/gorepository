// ex3.3
package main

import (
    "bufio"
    "fmt"
    "io"
    "log"
    "os"
    "strconv"
    "strings"
)

type INetwork interface {
    addAddress()
    deleteAddress()
}

type User struct {
    firstName  string
    secondName string
    lastName   string
    email      string
    url        string
    age        int
}

type MySite struct {
    INetwork
    users []*User
}

func (net *MySite) addAddress(user *User) []*User {
    net.users = append(net.users, user)
    return net.users
}
func (net *MySite) deleteAddress(email string) []*User {
    for i, user := range net.users {
        if user.email == email {
            copy(net.users[i:], net.users[i+1:])
            net.users[len(net.users)-1] = nil
            net.users = net.users[:len(net.users)-1]
            break
        }
    }
    return net.users
}
func (net *MySite) findByUrl(url string) *User {
    for _, user := range net.users {
        if user.url == url {
            return user
        }
    }
    return nil
}
func (net *MySite) findBySecondName(secName string) *User {
    for _, user := range net.users {
        if user.secondName == secName {
            return user
        }
    }
    return nil
}
func (net *MySite) download(fileName string) {
    file, err := os.Create(fileName)
    if err != nil {
        log.Fatal(err)
        fmt.Println(err)
        os.Exit(1)
    }
    defer file.Close()
    for _, user := range net.users {
        fmt.Fprintf(file, "%+v\n", *user)
    }
}
func (net *MySite) upload(fileName string) {
    file, err := os.Open(fileName)
    if err != nil {
        log.Fatal(err)
        fmt.Println("Unable to open file:", err)
        os.Exit(1)
    }
    defer file.Close()

    reader := bufio.NewReader(file)
    for {
        line, err := reader.ReadString('\n')
        if err != nil {
            if err == io.EOF {
                break
            } else {
                fmt.Println(err)
                return
            }
        }
        var lineTrim = strings.Trim(line, "{}")
        var lineSplit = strings.Split(lineTrim, " ")
        var user User
        for _, vocy := range lineSplit {
            field := strings.Split(vocy, ":")
            switch field[0] {
            case "firstName":
                user.firstName = field[1]
            case "secondName":
                user.secondName = field[1]
            case "lastName":
                user.lastName = field[1]
            case "email":
                user.email = field[1]
            case "url":
                user.url = field[1]
            case "age":
                user.age, err = strconv.Atoi(field[1])
            default:
                continue
            }
        }
        net.addAddress(&user)
    }
}
func (net *MySite) show() {
    for _, us := range net.users {
        fmt.Printf("%+v\n", *us)
    }
    fmt.Println()
}

func main() {
    site := new(MySite)

    site.upload("conf1.txt")
    // site.addAddress(&User{firstName: "Kevin", secondName: "Brouklin", lastName: "Mails", email: "kevinbrouk@gmail.com", url: "gmail.kevinbrouk.com", age: 25})
    // site.addAddress(&User{firstName: "Miki", secondName: "Downtan", lastName: "Tracy", email: "drowdath@gmail.com", url: "gmail.mikidowntan.com", age: 22})
    // site.addAddress(&User{firstName: "Lasy", secondName: "Papper", lastName: "Pots", email: "kmikidowntan@gmail.com", url: "gmail.lasypapper.com", age: 30})
    site.show()
    site.deleteAddress("drowdath@gmail.com")
    site.show()
    fmt.Println(*site.findByUrl("gmail.kevinbrouk.com"))
    fmt.Println(*site.findBySecondName("Papper"))

    //site.download("conf1.txt")
}
