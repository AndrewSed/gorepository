package main

import (
    "golang-book/chapter2/functions"
    "fmt"
)

func main() {
    args := []float64{22, 43, 90, 45, 35}
    fmt.Println("Sum", args, "=", functions.Sum(args...))
}
