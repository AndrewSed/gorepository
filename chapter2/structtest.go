package main

import (
	"fmt"
	// "os"
)
type INetwork interface {
	network()
	setDomain(string)
	addAddr(string)
	delAddr(string)
	show()
}

type Network struct {
	INetwork
	domain string
	addresses []*string
}

type Google struct {
	Network
}

type Yandex struct {
	Network
}

func (net *Network) network() *Network {
	return net
}

func (net *Network) setDomain(domain string) {
	net.domain = domain
}
func (net *Network) addAddr(address ...string) {
	for _, v := range address {
		net.addresses = append(net.addresses, &v) 
	}
}

/**
 Метод удаления email адресса.
*/
func (p *Network) delAddr(emailAddr string) []*string {
	for i, v := range p.emails {
        if *v == emailAddr {
            copy(p.emails[i:], p.emails[i+1:])
            p.emails[len(p.emails)-1] = nil
            p.emails = p.emails[:len(p.emails)-1]
        }
    }
    return p.emails
}

func (net *Network) show() {
	fmt.Println("{\n\tdomain:", net.domain)
	fmt.Println("\taddresses:\n\t{")
	for _, v := range net.addresses{
		fmt.Printf("\t\t%v\n", *v)
	}
	fmt.Println("\t}\n}")
}

type Address struct {
	firstName string
	secondName string
	lastName string
	age int
	networks []*Network
}

func (address *Address) show() {
	fmt.Println("{\n\tfirsName:", address.firstName)
	fmt.Println("\tsecondName:", address.secondName)
	fmt.Println("\tlastName:", address.lastName)
	fmt.Println("\tage:", address.age)
	fmt.Println("\tnetworks:")
	for _, addr := range address.networks {
		addr.show()
	}
	fmt.Println("}")
}

type Data struct {
	addresses []*Address
}

func initData() *Data {
	data := new(Data)

	var google = new(Google)
	google = &Google{Network{domain:"google.com"}}
	google.addAddr("suus@gmail.com", "su23us@gmail.com")
	var yandex = new(Yandex)
	yandex = &Yandex{Network{domain:"yandex.ru"}}
	yandex.addAddr("suus@yandex.ru")
	var networks []*Network
	networks = append(networks, google.network(), yandex.network())
	var address *Address
	address = &Address{"Andrew","Sedoykin","Alexandrovich",22,networks}
	data.addresses = append(data.addresses, address)

	google = new(Google)
	google = &Google{Network{domain:"google.com"}}
	google.addAddr("kevinbrouk@gmail.com")
	yandex = new(Yandex)
	yandex = &Yandex{Network{domain:"yandex.ru"}}
	yandex.addAddr("kevinbrouk@yandex.ru")
	networks = []*Network{}
	networks = append(networks, google.network(), yandex.network())
	address = &Address{"Kevin","Brouklin","Mails",25, networks}
	data.addresses = append(data.addresses, address)
	
	google = new(Google)
	google = &Google{Network{domain:"google.com"}}
	google.addAddr("drowdath@gmail.com")
	yandex = new(Yandex)
	yandex = &Yandex{Network{domain:"yandex.ru"}}
	yandex.addAddr("drowdath@yandex.ru")
	networks = []*Network{}
	networks = append(networks, google.network(), yandex.network())
	address = &Address{"Miki","Downtan","Tracy",23, networks}
	data.addresses = append(data.addresses, address)
	
	google = new(Google)
	google = &Google{Network{domain:"google.com"}}
	google.addAddr("kmikidowntan@gmail.com")
	yandex = new(Yandex)
	yandex = &Yandex{Network{domain:"yandex.ru"}}
	yandex.addAddr("kmikidowntan@yandex.ru")
	networks = []*Network{}
	networks = append(networks, google.network(), yandex.network())
	address = &Address{"Lasy","Papper","Pots",30, networks}
	data.addresses = append(data.addresses, address)

	return data
}

func main() {

	data := initData()
	for _, addr := range data.addresses {
		addr.show()
	}
}