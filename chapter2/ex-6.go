package main

import (
    "golang-book/chapter2/functions"
    "fmt"
)

func main() {
    pathDir := "/"

    files,err := functions.FindMaxFileSize(pathDir)
    if err != nil {
        fmt.Println(err)
    } else { 
        for _, f := range files {
            fmt.Printf("Name: \"%v\" Size: %v Kbs\n", f.Name(), f.Size())
        }
    }
}