package main

import (
    "fmt"
    "math"
    "golang-book/chapter2/geometry"
)

type Rectengele struct {
    geometry.IGeometry
    sides  [2]float64
}

type Triangle struct {
    geometry.IGeometry
    sides   [3]float64
}

type Circle struct {
    geometry.IGeometry
    r       float64
}

func (shape *Rectengele) Perimenter() float64 {
    var perimenter float64
    var sum float64
    for _, val := range shape.sides {
        sum += val
    }
    perimenter = sum * 2

    return perimenter
}

func (shape *Triangle) Perimenter() float64 {
    var perimenter float64 = 0
    for _, val := range shape.sides {
        perimenter += val
    }

    return perimenter
}

func (shape *Circle) Perimenter() float64 {
    var perimenter float64
    perimenter = 2 * math.Pi * shape.r

    return perimenter
}

func (shape *Rectengele) Area() float64 {
    var area float64
    area = shape.sides[0] * shape.sides[1]

    return area
}

func (shape *Triangle) Area() float64 {
    var area float64
    a := shape.sides[0]
    b := shape.sides[1]
    c := shape.sides[2]
    p := shape.Perimenter() / 2
    area = math.Pow( p * (p - a) * (p - b) * (p - c) , 0.5)

    return area
}

func (shape *Circle) Area() float64 {
    var area float64
    area = math.Pi * math.Pow(shape.r, 2)

    return area
}

func main() {
    sh1 := Rectengele{sides: [2]float64{2, 4}}
    fmt.Printf( "Area: %.2f, Pertr: %.2f" , sh1.Area(), sh1.Perimenter())
    fmt.Println()

    sh2 := Triangle{sides: [3]float64{3, 4, 2}}
    fmt.Printf( "Area: %.2f, Pertr: %.2f" , sh2.Area(), sh2.Perimenter())
    fmt.Println()
    
    sh3 := Circle{r: 4}
    fmt.Printf( "Area: %.2f, Pertr: %.2f" , sh3.Area(), sh3.Perimenter())
    fmt.Println()

}
