package main

import (
    "golang-book/chapter2/functions"
    "fmt"
    "time"
    // "sync"
    "math/rand"
)

func GenArray() []float64{
    const N int = 10000
    array := make([]float64, N)
    rand.Seed(time.Now().Unix())
    s := rand.NewSource(time.Now().Unix())
    r := rand.New(s)
    for i := 0; i < N; i++ {
        array = append(array, r.Float64() * 1000)
    }
    return array
}

func main() {
    
    array := GenArray()

    c1 := make(chan float64)
    c2 := make(chan float64)

    // var wg sync.WaitGroup
    // wg.Add(3)
    start := time.Now()
    go func() {
        // defer wg.Done()
        go func() {
            functions.Minimum(array, c1)
        }()
        go func() {
            functions.Maximum(array, c2)
        }()
    }()
    final := time.Now()
    elapsed := final.Sub(start)

    min, max := <-c1, <-c2
    fmt.Printf("min = %.2f\nmax = %.2f\ntime = %v\n\n", min, max, elapsed)

    start = time.Now()
    min = functions.Minimum(array)
    max = functions.Maximum(array)
    final = time.Now()
    elapsed = final.Sub(start)
    fmt.Printf("min2 = %.2f\nmax2 = %.2f\ntime2 = %v\n\n", min, max, elapsed)
}