package geometry

type IGeometry interface {
	Perimenter() float64
	Area() float64
}
