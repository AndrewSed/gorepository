//exes 2.1, 2.2
package main

import (
    "fmt"
    "os"
    "strings"
    "errors"
)

func OrderDirByImg(pathDir string) ([]os.FileInfo, error) {
    dir, err := os.Open(pathDir)
    var list []os.FileInfo

    if err != nil {
        err := errors.New("Error, failed to open dir")
        return list, err
    }
    defer dir.Close()

    fileInfos, err := dir.Readdir(-1)
    if err != nil {
        err := errors.New("Error, failed to read dir")
        return list, err
    }
    
    for _, fi := range fileInfos {
        if strings.HasSuffix(fi.Name(), ".png") || strings.HasSuffix(fi.Name(), ".jpeg") || strings.HasSuffix(fi.Name(), ".jpg") {
            list = append(list, fi)
        }
    }

    return list, err
}

func main() {
    var pathDir string = "/opt/yandex/browser-beta"

    var files, err = OrderDirByImg(pathDir)
    if err != nil {
        return
    }

    for _, file := range files {
        fmt.Println("Name:", file.Name(), "Size:", file.Size(), "Kb")
    }
}